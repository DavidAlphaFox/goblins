#lang racket/base

;; <-rall stands for "Resolve-all"

(provide <-rall)

(require "../core.rkt"
         "joiners.rkt"
         racket/match)

(define (<-rall actor-vow . arg-vows)
  (on (all-of* (cons actor-vow arg-vows))
      (match-lambda
        [(list actor args ...)
         (apply <- actor args)])
      #:promise? #t))

(module+ test
  (require rackunit
           "../vat.rkt"
           "bootstrap.rkt"
           racket/string)
  (define (^ident _bcom val)
    (lambda _ val))
  (define-vat-run vat-run (make-vat))
  (define apple-ident (vat-run (spawn ^ident "Apple")))
  (define banana-ident (vat-run (spawn ^ident "Banana")))
  (define pear-ident (vat-run (spawn ^ident "Pear")))
  (define (^chef _bcom)
    (lambda ingredients
      (format "To make zee recipe, combine zee ~a"
              (string-join ingredients ", "))))
  (define (^chef-maker _bcom)
    (lambda _ (spawn ^chef)))
  (define chef-maker (vat-run (spawn ^chef-maker)))

  (define (test-vow channel vow)
    (on vow
        (lambda (chef-sez)
          (channel-put channel `(result ,chef-sez)))
        #:catch
        (lambda (err)
          (channel-put channel `(err ,(exn-message err))))))

  (define result-ch (make-channel))
  (vat-run
   (define apple-vow (<- apple-ident))
   (define banana-vow (<- banana-ident))
   (define pear-vow (<- pear-ident))
   (define chef-vow (<- chef-maker))
   (define combined-vow
     (<-rall chef-vow apple-vow banana-vow pear-vow))
   (test-vow result-ch combined-vow))

  (check-equal?
    (sync/timeout 1 result-ch)
    '(result "To make zee recipe, combine zee Apple, Banana, Pear"))

  (define (^bad-ident _bcom val)
    (lambda _ (error val)))

  (vat-run
   (define bad-orange-ident (spawn ^bad-ident "Orange"))
   (define apple-vow (<- apple-ident))
   (define bad-orange-vow (<- bad-orange-ident))
   (define chef-vow (<- chef-maker))

   (define combined-vow
     (<-rall chef-vow apple-vow bad-orange-vow))
   (test-vow result-ch combined-vow))

  (check-equal?
   (sync/timeout 1 result-ch)
   (list 'err "Orange")))
