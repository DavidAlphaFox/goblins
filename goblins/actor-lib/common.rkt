#lang racket/base

;;; Copyright 2020-2021 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(provide ^hash ^hasheq ^hasheqv
         ^filo-queue
         ^set ^seteq ^seteqv
         ^incrementer)

(require "../core.rkt"
         "methods.rkt"
         racket/set
         racket/sequence)

(define (^hash bcom [ht #hash()])
  (methods
   [ref
    (case-lambda
      [(key)
       (hash-ref ht key)]
      [(key dflt)
       (hash-ref ht key dflt)])]
   [(set key val)
    (bcom (^hash bcom (hash-set ht key val)))]
   [(has-key? key)
    (hash-has-key? ht key)]
   [(remove key)
    (bcom (^hash bcom (hash-remove ht key)))]
   [(keys) (hash-keys ht)]
   [(values) (hash-values ht)]
   [(data) ht]))

;; Really just sets up a new hasheq
;; Doesn't check it but maybe it should.
(define (^hasheq bcom)
  (^hash bcom #hasheq()))

(define (^hasheqv bcom )
  (^hash bcom #hasheqv()))

(define (^filo-queue bcom [lst '()])
  (methods
   [(empty?)
    (null? lst)]
   [(push item)
    (bcom (^filo-queue bcom (cons item lst)))]
   [(pop)
    (when (null? lst)
      (error "Tried to pop empty queue"))
    (bcom (^filo-queue bcom (cdr lst))
          (car lst))]
   [(append new-lst)
    (bcom (^filo-queue bcom (append new-lst lst)))]
   [(data) lst]
   [(remove item #:same? [same? equal?])
    (bcom (^filo-queue bcom (remove item lst same?)))]))

(define (^set bcom [s (set)])
  (methods
   [(add val)
    (bcom (^set bcom (set-add s val)))]
   [(remove val)
    (bcom (^set bcom (set-remove s val)))]
   [(member? val)
    (set-member? s val)]
   [(length)
    (sequence-length s)]
   [(data) s]))

(define (^seteq bcom)
  (^set bcom (seteq)))

(define (^seteqv bcom)
  (^set bcom (seteqv)))

(define (^incrementer bcom [val 0]
                      #:step [step 1])
  (methods
   [(incr)
    (bcom (^incrementer bcom (+ val step)
                        #:step step))]
   [(val)
    val]))

(module+ test
  (require rackunit)

  (define am (make-actormap))

  (define ht
    (actormap-spawn! am ^hasheq))

  (actormap-poke! am ht 'set 'foo 1)
  (actormap-poke! am ht 'set 'bar 2)
  
  (check-equal? (actormap-peek am ht 'data)
                #hasheq((foo . 1)
                        (bar . 2)))

  (actormap-poke! am ht 'remove 'foo)
  
  (check-equal? (actormap-peek am ht 'data)
                #hasheq((bar . 2)))

  (define s
    (actormap-spawn! am ^seteq))

  (actormap-poke! am s 'add 'foo)
  (actormap-poke! am s 'add 'bar)
  (check-equal? (actormap-peek am s 'data) (seteq 'foo 'bar))

  (define q
    (actormap-spawn! am ^filo-queue))
  (actormap-poke! am q 'push 'beep)
  (actormap-poke! am q 'push 'boop)
  (check-equal? (actormap-peek am q 'data) '(boop beep))
  (check-equal? (actormap-poke! am q 'pop) 'boop)
  (check-equal? (actormap-peek am q 'data) '(beep))

  (define incr
    (actormap-spawn! am ^incrementer))
  (check-equal? (actormap-peek am incr 'val) 0)
  (actormap-poke! am incr 'incr)
  (actormap-poke! am incr 'incr)
  (check-equal? (actormap-peek am incr 'val) 2)
  )
