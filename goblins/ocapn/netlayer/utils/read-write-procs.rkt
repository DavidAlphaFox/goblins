#lang racket/base

(provide read-write-procs)

(require syrup)

(define (read-write-procs ip op)
  (define (read-message unmarshallers)
    (syrup-read ip #:unmarshallers unmarshallers))
  (define (write-message msg marshallers)
    (syrup-write msg op #:marshallers marshallers)
    (flush-output op))
  (values read-message write-message))
