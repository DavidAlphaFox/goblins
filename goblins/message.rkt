#lang racket/base

(provide (struct-out message)
         (struct-out questioned)
         (struct-out listen-request))

;; These are the main things that get sent as the toplevel of a turn in a vat!
(struct message (to resolve-me kws kw-vals args)
  #:transparent)

;; When speaking to the captp connector, sometimes we're really asking
;; a question.
(struct questioned (message answer-this-question)
  #:transparent)

;; Sent in the same way as <message>, but does listen requests specifically
(struct listen-request (to listener wants-partial?)
  #:transparent)
